<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBikramMobileTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->string('lat_lng');
            $table->string('contact_number');
            $table->timestamps();
        });

        Schema::create('mobiles', function( Blueprint $table ){
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('daily_sales', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
            $table->integer('mobile_id')->unsigned();
            $table->foreign('mobile_id')->references('id')->on('mobiles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
