<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Entities\Task::class, function (Faker\Generator $faker)
{
    $types = array_keys(App\Entities\Task::$type);
    $type = $types[array_rand($types)];

    $repeats = array_values(App\Entities\Task::$repeat);
    $repeat = $repeats[array_rand($repeats)];

    $users = array_values(App\Entities\Task::$users);
    $user = $users[array_rand($users)];

    $time = range(1,100);
    $time = $time[array_rand($time)];

    return [
        'type' => $type,
        'lastname' => $faker->lastName,
        'firstname' => $faker->firstName,
        'address' => $faker->address,
        'postal_code' => $faker->postcode,
        'phone' => $faker->phoneNumber,
        'mail' => $faker->email,
        'time_to_finish' => $time,
        'place' => $faker->word,
        'worker' => $user,
        'repeat' => $repeat,
        'notice' => $faker->word,
        'status' => 0
    ];
});

$factory->define(App\Entities\Resource::class, function(Faker\Generator $faker){
    $task = App\Entities\Task::all()->random()->first();

    $types = ['image','pdf','video'];

    return [
        'task_id' => $task->id,
        'type' => $types[array_rand($types)],
        'name' => $faker->name
    ];
});

$factory->define(App\Entities\Reminder::class, function(Faker\Generator $faker){
    return [
        'title' => $faker->sentence,
        'status' => 0
    ];
});