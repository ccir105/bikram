<?php

use Illuminate\Database\Seeder;

class GardenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Entities\Task::class,10)->create();
        factory(App\Entities\Resource::class,10)->create();
        factory(App\Entities\Reminder::class,10)->create();
    }
}
