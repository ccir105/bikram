<?php

/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 3/17/16
 * Time: 4:59 PM
 */
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Mobile;
use App\Shop;
use App\Sales;


class TaskRepoTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
    }

    public function test_initData(){
        $response = $this->call('GET','/api/init-data');

        $res = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('data', $res);

        $res = $res['data'];
        
        $this->assertArrayHasKey('mobiles', $res);
        $this->assertArrayHasKey('shops', $res);
    }

    public function test_make_viber()
    {
        $shops = App\Shop::all();
                
        $sales = App\Sales::where(DB::raw('MONTH(created_at)'), '=', date('n'))->get();

        $mobiles = App\Mobile::all(); 

        $response = [];

        foreach( $shops as $shop )
        {
            $thisMonthSales = $sales->filter(function($sale)use($shop){
                return $sale->shop_id == $shop->id;
            })->toArray();

            $todaySales = $sales->filter(function($sale)use($shop){

                if( $sale->shop_id == $shop->id )
                {
                    $baseDate = date('Y-m-d',strtotime($sale->created_at));
                    $today = date('Y-m-d');
                    if($today == $baseDate)
                    {
                        return $shop;
                    }
                }

            })->toArray();

            $monthTotal = array_sum( array_map(function($a){ return $a['quantity']; } , $thisMonthSales ) );

            $todayTotal = array_sum( array_map(function($a){ return $a['quantity']; } , $todaySales ) );

            $response[] = ['monthly' => $monthTotal,'today'=> $todayTotal, 'shop_name' => $shop->name,'shop_id' => $shop->id, 'today_sales' => $todaySales ,'month_sales' => $thisMonthSales];
        }

        $montlyAllMobile = [];
        $dailyMobile = [];

        foreach( $response as &$res )
        {
            foreach( $mobiles as $mobile )
            {
                $monthlySales = $res['month_sales'];

                $monthlyMobile = array_filter($monthlySales, function($m)use($mobile){
                    return $m['mobile_id'] == $mobile->id; 
                });

                $res['mobiles_monthly'][] = [ 'mobile_id' => $mobile->id, 'name' => $mobile->name,'total' => array_sum( array_map(function($a){ return $a['quantity']; } , $monthlyMobile ) ) ];

                $todayMobile = array_filter($res['today_sales'], function($m)use($mobile){
                    return $m['mobile_id'] == $mobile->id;
                });

                $res['mobiles_today'][] = ['name' => $mobile->name,'mobile_id' => $mobile->id, 'total' => array_sum( array_map(function($a){ return $a['quantity']; } , $todayMobile ) )];
            }

            unset($res['today_sales']);
            unset($res['month_sales']);
        }

        foreach($response as $res)
        {
            foreach ($res['mobiles_monthly'] as $model) 
            {
                $monthlyMobile[ $model['mobile_id'] ][] = $model;
            }

            foreach ($res['mobiles_today'] as $model) 
            {
                $dailyMobile[ $model['mobile_id'] ][] = $model;
            }
        }

        $todayModelTotal = array_sum(array_map( function($model){  return array_sum(array_column( $model, 'total' )); },$dailyMobile ) );

        $dailyMobileSales = [];
        $monthlyMobileSales = [];

        foreach($dailyMobile as $model)
        {
            $total = array_sum(array_column( $model, 'total' ));
            $dailyMobileSales[$model['id']] = ['model_name' => $model[0]['name'] , 'total' => $total];
        }

        foreach($monthlyMobile as $model )
        {
            $total = array_sum(array_column( $model, 'total' ));
            $monthlyMobileSales[$model['id']] = ['model_name' => $model[0]['name'] , 'total' => $total];
        }


        $this->assertEquals(22, array_sum( array_map(function($m){ return $m['total']; }, $monthlyMobileSales)));

        // $starMobile = array_filter($response, function($r){
        //     return $r['shop_id'] == 9;
        // });

        // $starMobile = array_values($starMobile)[0];

        // $this->assertEquals(6, $starMobile['monthly']);

        // $this->assertEquals(6, $starMobile['today']);

        // $todayMobiles = $starMobile['mobiles_today'];

        // $ab = array_filter($todayMobiles, function($r){
        //     return $r['mobile_id'] == 13;
        // });

        // $this->assertEquals(1, count($ab));

        // $ab = array_values($ab)[0];

        // $this->assertEquals(1, $ab['total']);
    }
}