<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repos\Interfaces\Task as TaskInterface;
use RepeatHelper;

class RepeatingTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repeat:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Repeats task based on repeat date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(TaskInterface $task)
    {
        $lists = $task->getTodayRepeatingTask();

        if( !$lists->isEmpty() )
        {
            foreach( $lists as $list)
            {
                $this->info('Reating task of ' . $list->id);
                
                RepeatHelper::onRepeatingDay( $list );
            }
        }

        $this->info('Not any task to repeat');
    }
}
