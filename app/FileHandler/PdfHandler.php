<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 3/18/16
 * Time: 11:12 PM
 */

namespace App\FileHandler;


class PdfHandler extends HandlerContact
{

    public function afterUpload($path, $name, $file)
    {
        return ['thumb' => 'pdf-default.png'];
    }
}