<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 3/18/16
 * Time: 11:11 PM
 */

namespace App\FileHandler;
use Image;

class ImageHandler extends HandlerContact
{

    public function afterUpload( $path, $name, $file)
    {
        $fileName = $name['name'] . '-thumbnail.' . $name['ext'];

        $path = public_path('uploads/'.$fileName);

        $name = implode('.',array_values($name));

        Image::make(storage_path('app/'. $name))

        ->resize(100, 100 ,function($constraint)
        {
            $constraint->aspectRatio();
        })->save($path);

        return ['thumb'=> $fileName];
    }
}