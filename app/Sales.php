<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = 'daily_sales';

    protected $fillable = ['mobile_id','shop_id','hidden','quantity','customer_name','phone_number','imei_number'];

    public function mobile(){
    	return $this->belongsTo('App\Mobile');
    }

    public function shop(){
    	return $this->belongsTo('App\Shop');
    }
}
