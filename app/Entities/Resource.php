<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\FileHandler\HandlerContact as FileHandler;

class Resource extends Model
{
    protected $table = "resources";

    protected $fillable = ['type','name','thumb','task_id','client_name'];

    protected $appends = ['download_path'];

    public function task()
    {
        return $this->belongsTo('App\Entities\Task','task_id','id');
    }

    public function getThumbAttribute( $value )
    {
        return FileHandler::getThumb( $value );
    }

    public function getDownloadPathAttribute()
    {
        return FileHandler::getDownloadPath( $this->name );
    }
}
