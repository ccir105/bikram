<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{

    protected $table = 'reminders';

    protected $fillable = ['title'];
}