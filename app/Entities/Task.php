<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = "tasks";

    const MAINTAIN = 1;

    const CHANGES = 2;

    protected $fillable = ['type', 
        'lastname',
        'firstname',
        'address', 
        'postal_code',
        'phone', 
        'mail', 
        'time_to_finish', 
        'worker',
        'repeat',
        'notice',
        'execution_time',
        'place'
    ];

    public static $type = [
        self::MAINTAIN => "Gartenumaenderung",
        self::CHANGES => "Gartenunterhalt"
    ];

    public static $repeat = [
        '0' =>'Einmalig',
        '1-1' => 'Täglich',
        '5-2' => 'Wöchentlich',
        '21-7' => 'Monatlich',
        '335-30' => 'Jährlich'
    ];

    const FirstUser = 1;

    const SecondUser = 2;

    const ThirdUser = 3;

    public static $users = [
        self::FirstUser => 'Thomas Müller',
        self::SecondUser => 'Niklas Bartels',
        self::ThirdUser => 'Manuela Ludwig'
    ];

    public function getTypeAttribute( $value )
    {
        return self::$type[ $value];
    }

    public function resources()
    {
        return $this->hasMany('App\Entities\Resource','task_id','id');
    }

    public function getRepeatKey( $value )
    {
        return array_search( $value, self::$repeat );
    }
}