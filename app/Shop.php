<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table = 'shops';

    protected $fillable = ['name','address','contact_number','lat_lng','isp_name','area_id'];

    public function sales(){
    	return $this->hasMany('App\Sales','shop_id','id');
    }

    public function mobiles()
    {
    	return $this->hasManyThrough('App\Sales','App\Mobile');
    }

    public function area()
    {
    	return $this->belongsTo('App\Area','area_id','id');
    }
}
