<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 3/19/16
 * Time: 12:14 PM
 */

namespace App\Repos\Eloquent;
use App\Repos\Interfaces\Resource as ResourceInterface;
use App\Entities\Resource as ResourceModel;
use App\FileHandler\HandlerContact as FileHandler;
use Storage;

class Resource implements ResourceInterface
{
    protected $resource;

    public function __construct()
    {
        $this->resource = new ResourceModel;
    }

    public function save( $data )
    {
        return $this->resource->create( $data );
    }

    public function clear( $task )
    {
        $this->resource->where('task_id',$task->id)->delete();
    }

    public function findByTask($task)
    {
       return $this->resource->where('task_id',$task->id)->get();
    }

    public function saveTask($task, $resources)
    {
        if( $ids = $this->findResources( $resources ) )
        {
            return $this->resource->whereIn('id', $ids)->update(['task_id' => $task->id]);
        }
    }

    public function findResources( $names)
    {
        $validFiles = [];

        foreach($names as $name)
        {
            if( FileHandler::exists($name) )
            {
                array_push($validFiles,$name);
            }
        }

        if(count($validFiles) != 0)
        {
            $resources = $this->resource->whereIn('name',$names)->select('id')->get();

            if( !$resources->isEmpty() )
            {
                return $resources->lists('id')->toArray();
            }
        }

        return false;
    }

    public function updateByTask( $task, $resources )
    {
        $dbResources = $this->findByTask( $task )->lists( 'name' )->toArray();

        $newResource = array_diff( $resources, $dbResources);

        if( count( $newResource ) !== 0 )
        {
            $newResource = array_values( $newResource );

            $this->saveTask( $task, $newResource );
        }

        $toRemove = array_diff( $dbResources, $resources );

        if( count( $toRemove ) !== 0 )
        {
            $toRemove = array_values( $toRemove );

            $this->resource->whereIn('name',$toRemove)->delete();

            FileHandler::delete($toRemove);
        }
    }
}