<?php namespace App\Repos\Eloquent;
use App\Entities\Resource;
use App\Repos\Interfaces\Reminder as ReminderInterface;
use App\Entities\Reminder as ReminderModel;

class Reminder implements ReminderInterface
{
    protected $model;

    function __construct()
    {
        $this->model = new ReminderModel();
    }

    function find( $model )
    {
        return is_object($model) ? $model : $this->model->find( $model );
    }

    function all()
    {
        return $this->model->where('status',0)->latest()->get();
    }

    function getAllDone($total = 10, $query = null)
    {
        $reminders = $this->model->whereStatus(1)->orderBy('updated_at','desc');

        if( !is_null($query) )
        {
            $reminders->whereRaw("title LIKE '$query%'");
        }
        return $reminders->paginate($total);
    }

    function save( $data , $model = null )
    {
        $model = ( is_null( $model ) ) ? $this->model : $model;

        $model->fill($data);

        $model->save();

        $model = $model->fresh();

        return $model;
    }

    function delete($model)
    {
        return $model->delete();
    }

    function changeStatus( $model , $status )
    {
        if($model->status == $status)
        {
            return false;
        }

        $model->status = $status;

        return $model->save();
    }

    function paginate( $total = 10 )
    {
        return $this->model->latest()->whereStatus(0)->paginate( $total );
    }
}