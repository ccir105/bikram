<?php namespace App\Repos\Eloquent;

use App\Repos\Interfaces\Resource as ResourceInterface;
use App\Repos\Interfaces\Task as TaskInterface;
use App\Entities\Task as TaskModel;
use App\Entities\Resource;
use RepeatHelper;

class Task implements TaskInterface
{
    const MAINTAIN = 1;

    const CHANGES = 2;

    protected  $model;

    protected $resourceRepo;

    public function __construct()
    {
        $this->model = new TaskModel();

        $this->resourceRepo = app()->make('App\Repos\Interfaces\Resource');
    }

    public function getTypes()
    {
        return TaskModel::$type;
    }

    public function setInstance($model)
    {
        $this->model = $model;
    }

    function find( $model )
    {
        if( is_numeric( $model ) )
        {
            return $this->model->find( $model );
        }

        $resources =  Resource::where('task_id', $model->id)->get();
        
        $thumbs = [];

        foreach ($resources as $resource)
        {
            $thumbs[] = [
                'name' => $resource->name,
                'extension' => pathinfo($resource->name,PATHINFO_EXTENSION),
                'thumb' => $resource->thumb,
                'client_name' => $resource->client_name
            ];
        }
        
        $model->thumbs = $thumbs;

        $model->resources = $resources->lists('name');
        
        return $model;
    }

    function all()
    {
        return $this->model->whereStatus('0')->latest()->get();
    }

    function getByType($type)
    {
        return $this->model->whereType($type)->whereStatus('0')->latest()->paginate(10);
    }

    function getAllDone($type, $query = null)
    {
        $returnData = [];

        $tasks = $this->model->whereStatus(1)->whereType($type)->orderBy('updated_at','desc');

        if(!is_null($query))
        {
            $column = ['firstname','lastname','address','mail','postal_code','phone'];

            foreach($column as $col)
            {
                $sql[] = $col . ' LIKE ' . "'$query%'";
            }

            $sql = implode(' or ', $sql);

            $tasks->whereRaw($sql);
        }

        $tasks = $tasks->get();

        return $this->groupByYear( $tasks );
    }

    function groupByYear( $tasks )
    {
        $returnData = [];

        $tasks = $tasks->groupBy( function( $date ) {
            return $date->created_at->format('Y');
        });

        foreach ($tasks as $year => $list)
        {
            $returnData[] = array(
                'year' => $year,
                'projects' => $list
            );
        }

        return $returnData;
    }

    function save( array $data, $task = null )
    {
        $taskModel = ( is_null($task ) ) ? $this->model : $task;

        $taskModel->fill($data);

        $taskModel->repeat_code = $this->model->getRepeatKey( $taskModel->repeat );

        $taskModel->save();

        $taskModel->load('resources');

        if(isset( $data['resources'] ) and is_array( $data['resources'] ) )
        {
            if ( is_null($task) || $taskModel->resources->isEmpty() )
            {
                $this->resourceRepo->saveTask( $taskModel, $data['resources'] );
            }
            else
            {
                $this->resourceRepo->updateByTask( $taskModel , $data['resources'] );
            }
        }

        return $taskModel->fresh();
    }

    function changeStatus($task, $status)
    {
        if( $task->status == $status )
        {
            return false;
        }

        $task->status = $status;

        if( $status == '1' )
        {
            return RepeatHelper::afterCompletion( $task );
        }
        else
        {
            return $task->save();
        } 

    }

    function delete( $task )
    {
        return $task->delete();
    }

    function getUsers()
    {
        return TaskModel::$users;
    }

    function getRepeatTypes()
    {
        return TaskModel::$repeat;
    }

    function addToCalender($task)
    {
        if($task->added_to_calender == 1)
        {
            return false;
        }

        $task->added_to_calender = 1;

        return $task->save();
    }

    function getTodayRepeatingTask()
    {
        return $this->model->where('status', 1)->whereRaw( 'next_repeat BETWEEN "'.date('Y-m-d H:i:s').'" and ( "'.date('Y-m-d H:i:s').'" + INTERVAL 1 HOUR )' )->get();
    }

    function makeDuplicate( $task )
    {
        $task = $this->find( $task );
        
        $newTask['resources'] = $task->resources->toArray();
        
        $newTask['type'] = array_search( $task->type, $task::$type );
        
        $oldModel = $task->toArray();

        $oldModel = array_merge($oldModel, $newTask);

        $newTask = $this->save($oldModel);

        $newTask->due_date = $task->due_date;
        
        $newTask->execution_time = $task->due_date;

        $newTask->save();

        return $newTask;
    }

    function afterTaskCompletion( $task, $dueDate )
    {
        if( is_array( $dueDate ) )
        {
            $task->next_repeat = $dueDate['repeat_date'];

            $task->due_date = $dueDate['due_date'];
        }
        
        $task->completed_date = date( 'Y-m-d H:i:s' );
        
        $task->save();

        return $task;
    }
}