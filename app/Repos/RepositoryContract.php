<?php namespace App\Repos;

abstract class RepositoryContract{

    protected $model;

    public function __construct($model = null)
    {
        $this->model = $model;
    }

    public function setInstance($model)
    {
        $this->model = $model;
    }
}