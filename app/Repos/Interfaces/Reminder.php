<?php namespace App\Repos\Interfaces;


interface Reminder
{
    function find($model);

    function all();

    function getAllDone($total = 10);

    function save( $data , $model = null);

    function delete($model);

    function changeStatus($model, $status );

    function paginate($total = 10);
}