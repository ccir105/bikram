<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 3/19/16
 * Time: 12:12 PM
 */

namespace App\Repos\Interfaces;


interface Resource
{
    public function save($data);

    public function clear($task);

    public function findByTask($task);

    public function saveTask( $task , $resources);

    public function findResources($names);

    public function updateByTask($task, $resources);
}