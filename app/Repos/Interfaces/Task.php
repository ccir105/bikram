<?php namespace App\Repos\Interfaces;
use App\Repos\Interfaces\Resource as ResourceInterface;

interface Task
{
    function find($task);

    function all();

    function getByType($type);

    function getAllDone($type,$query = null);

    function save(array $data, $task = null);

    function changeStatus($task, $status);

    function delete($task);

    function getTypes();

    function getUsers();

    function getRepeatTypes();

    function addToCalender($task);

    function getTodayRepeatingTask();
}