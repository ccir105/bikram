<?php

/**
 * @SWG\Swagger(
 *     basePath="/api",
 *     host="garden.app",
 *     schemes={"http"},
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Garden Api",
 *         description="A api documentation for garden app",
 *         termsOfService="http://helloreverb.com/terms/",
 *         @SWG\Contact(name="Swiss Magic"),
 *         @SWG\License(name="Sishir")
 *     )
 * )
 */

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
