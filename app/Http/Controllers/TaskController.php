<?php

namespace App\Http\Controllers;

use App\FileHandler\HandlerFactory as ResourceHandler;
use Illuminate\Http\Request;
use Res;
use App\Sales;
use App\Mobile;
use App\Shop;
use DB;
use App\Area;
use Session;

class TaskController extends Controller
{
   public function addSales(Request $request, $entity = null)
   {
      $sales = is_null($entity) ? new Sales() : $entity;
      $sales->fill($request->all());
      $sales->save();

      if($request->has('sales_date')){
        $sales->created_at = date('Y-m-d H:i:s', strtotime($request->get('sales_date')));
        $sales->save();
      }

      return Res::success($sales->load(['shop','mobile']));
   }

   public function addMobile(Request $request, $entity = null)
   {
      $mobile = is_null($entity) ? new Mobile() : $entity;
      $mobile->fill($request->all());
      $mobile->save();
      return Res::success($mobile);
   }

   public function addShop(Request $request, $entity = null)
   {
      $shop = is_null($entity) ? new Shop() : $entity;
      $shop->fill($request->all());
      $shop->save();
      $shop->load('area');
      return Res::success($shop);
   }

   public function initData()
   {
      $response['mobiles'] = Mobile::all();
      $response['areas'] = Area::all();
      $response['shops'] = Shop::with('area')->get();

      return Res::success($response);
   }

   public function getMobiles()
   {
        return Res::success(Mobile::all());
   }

   public function getShops()
   {
        return Res::success( Shop::all() );
   }

   public function searchSales(Request $request)
   {
        $shopQuery = Sales::select('*')->with('mobile','shop')->orderBy('daily_sales.created_at', 'desc');

        if( $request->has('mobile') )
        {
            $shopQuery->where('mobile_id',$request->get('mobile'));
        }

        if( $request->has('shop') )
        {
            $shopQuery->where('mobile_id',$request->get('shop'));
        }

        if( $request->has('month') )
        {
            $shopQuery->where(DB::raw('MONTH(created_at)'), '=', date('n')); 
        }

        return Res::success($shopQuery->get());
   }


   public function saveArea(Request $request, Area $area = null)
   {
      $area = is_null($area) ? new Area : $area;
      $area->fill($request->all());
      $area->save();

      return Res::success($area);
   }

   public function deleteArea(Area $area)
   {
      $area->delete();
      return Res::success();
   }


   public function deleteShop(Shop $area)
   {
      $area->delete();
      return Res::success();
   }


   public function deleteMobile(Mobile $area)
   {
      $area->delete();
      return Res::success();
   }

   public function deleteSales(Sales $area)
   {
      $area->delete();
      return Res::success();
   }

   public function viberMessage(Area $area = null){

        $shops = $area->shops;

        $hasDate = request()->has('date') ? date('Y-m-d', strtotime( request()->get('date') ) ) : date('Y-m-d');

        $sales = Sales::whereIn('shop_id',$shops->lists('id')->toArray());

        $sales->where( DB::raw('MONTH(created_at)'),'=', date('m', strtotime($hasDate) ) ); 

        $sales = $sales->get();

        $mobiles = Mobile::all();

        $thisMonth = array_sum($sales->lists('quantity')->toArray());

        $todaySales = $sales->filter(function($item)use($hasDate){
            $salesDate = date('Y-m-d', strtotime($item->created_at));
            return $salesDate === $hasDate;
        });

        $finalShop = [];

        $responseStr = 'Date: ' . date('M d Y', strtotime($hasDate)) . '<br>Area: ' .$area->name. '<br><br>';

        foreach( $shops as $key => $shop )
        {
            $shopDailySales = $todaySales->filter(function($s) use($shop){
                return $s->shop_id === $shop->id;
            });

            $_todaySales = array_sum($shopDailySales->lists('quantity')->toArray());

            $shopMonthlySales = $sales->filter(function($s)use($shop){
                return $s->shop_id === $shop->id;
            });

            $monthlySales = array_sum($shopMonthlySales->lists('quantity')->toArray());

            $responseStr .= '(' . ($key + 1) . ') '  . $shop->name .  ' ' . $_todaySales . '/' . $monthlySales .'<br>Isp: ('. ( $shop->isp_name ? $shop->isp_name : ' Non Isp ' ) .')<br>';

            foreach($mobiles as $mobile)
            {
                $mobileDaily = $shopDailySales->filter(function($i) use($mobile){
                  return $i->mobile_id === $mobile->id;
                });

                $totalMobileDaily = array_sum($mobileDaily->lists('quantity')->toArray());

                $mobileMonthly = $shopMonthlySales->filter(function($i) use($mobile){
                  return $i->mobile_id === $mobile->id;
                });

                $totalMobileMonthly = array_sum($mobileMonthly->lists('quantity')->toArray());

                $responseStr .= '&nbsp;&nbsp;&nbsp;'.$mobile->name  . ":&nbsp;&nbsp;&nbsp;" . $totalMobileDaily . '/' . $totalMobileMonthly . '<br>';
            }

            $responseStr .= '<br><br><br>';
        }

        $today = array_sum( $todaySales->lists('quantity')->toArray() );

        foreach($mobiles as $mobile)
        {
            $todayThisMobile = array_sum($todaySales->filter(function($i)use($mobile){
              return $i->mobile_id === $mobile->id;
            })->lists('quantity')->toArray());

           $monthThisMobile = array_sum($sales->filter(function($i)use($mobile){
              return $i->mobile_id === $mobile->id;
            })->lists('quantity')->toArray());

           $responseStr .=$mobile->name . ': ' . $todayThisMobile . '/' . $monthThisMobile . '<br>';
        }

        $responseStr .= '<br>Total Today: ' . $today . '<br>' . 'Total Monthly: ' . $thisMonth; 
       return Res::success(['viber' => $responseStr]);
   }

   public function ping(Request $request){
      
      if( $request->has('domain') )
      {
          $domain = $request->get('domain');
          
          if( preg_match('/[\w-]+\.[a-z]{2,}/', $domain) )
          {
            $command = 'ping -c 4 ' . $domain; 
            $this->performRealTimePing($command);
          }
      }
   }

   public function performRealTimePing($cmd){
      
      $descriptorspec = array(
         0 => array("pipe", "r"),   
         1 => array("pipe", "w"),   
         2 => array("pipe", "w")
      );

      flush();

      $realTimeProcess = proc_open($cmd, $descriptorspec, $pipes, realpath('./'), array());      

      if (is_resource( $realTimeProcess ) ) {

          while ($s = fgets($pipes[1])) {

              $this->handleOutput($s);

              flush();
          }
      }
            
      proc_close($realTimeProcess);
   }

   public function handleOutput($output)
   {
      event(new \App\Events\EventBroadcaster(['output' => $output,'to' => Session::get('access_token')],'command:output'));
   }
}