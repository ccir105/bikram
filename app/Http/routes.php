<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
    Route::group(['middleware' => ['web'] ], function() {

        Route::controllers([
            'auth' => 'Auth\AuthController',
            'password' => 'Auth\PasswordController',
        ]);

        Route::get('ping', function()
        {
            Session::put('access_token', str_random( 10 ) );
            
            $token = Session::get('access_token');

            event(new App\Events\EventBroadcaster(['token' => $token],'token:request'));

            return view('ping')->withToken($token);
        });

        Route::post('ping', 'TaskController@ping');

        Route::post('login', function(Illuminate\Http\Request $request){

            if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')], true ) ) {
                return ['success'=>true,'homeUrl'=> url("/")];
            }

            return ['success' => false ];
        });

        Route::group(['middleware' => ['auth']], function(){
            Route::get('/', function(){
                return view('index');
            });

            Route::get('logout', function(){
                Auth::logout();
                return redirect('/');
            });
        });

        Route::group(['prefix' => 'api'], function () {
        	Route::get('init-data','TaskController@initData');
        	Route::post('sales/{sales?}','TaskController@addSales');
        	Route::post('mobiles/{mobile?}','TaskController@addMobile');
        	Route::post('shops/{shop?}','TaskController@addShop');
        	Route::post('search-sales', 'TaskController@searchSales');
            Route::get('viber/{area?}', 'TaskController@viberMessage');
        	Route::get('viber-temp/{area?}', 'TaskController@_viberMessage');


        	Route::post('areas/{area?}','TaskController@saveArea');

        	Route::delete('areas/{area?}','TaskController@deleteArea');
        	Route::delete('shops/{shop?}','TaskController@deleteShop');
        	Route::delete('mobiles/{mobile}','TaskController@deleteMobile');
        	Route::delete('sales/{sales?}','TaskController@deleteSales');
        });
        
    });

    Route::bind('area', function( $areaId ){

    	$area = App\Area::find( $areaId );
    	
    	if( $area )
    	{
    		return $area;
    	}

    	throw new Illuminate\Database\Eloquent\ModelNotFoundException;
    });

     Route::bind('sales', function($saleId){
    	$sale = App\Sales::find($saleId);
    	if($sale)
    	{
    		return $sale;
    	}

    	throw new Illuminate\Database\Eloquent\ModelNotFoundException;
    });

      Route::bind('mobile', function($mobileId){
    	$mobile = App\Mobile::find($mobileId);
    	if($mobile)
    	{
    		return $mobile;
    	}

    	throw new Illuminate\Database\Eloquent\ModelNotFoundException;
    });

       Route::bind('shop', function($shopId){
    	$shop = App\Shop::find($shopId);

    	if($shop)
    	{
    		return $shop;
    	}

    	throw new Illuminate\Database\Eloquent\ModelNotFoundException;
    });