<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TaskResourceUploadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        \Validator::extend('file_upload',function($attributes, $value, $parameters)
        {
            if( $mime = $value->getMimeType() )
            {   
                if(preg_match('/image|pdf|video|audio/', $mime ) )
                {
                    return true;
                }
            }

        },'Cannot Upload This Type Of file');

        return [
            'project_file' => 'required|file_upload'
        ];
    }
}
