<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Entities\Task;
class ProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' => 'string',
            'firstname' => 'string',
            'address' => 'string',
            'postal_code' => 'string',
            'phone' => 'string',
            'mail' => 'email',
            'time_to_finish' => "integer|",
            'worker' => "in:" . implode(',' ,array_values( Task::$users ) ),
            'repeat' => "in:" . implode(',' ,array_values( Task::$repeat ) ),
        ];
    }
}
