<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 4/9/16
 * Time: 7:07 PM
 */

namespace App\Facade;


use Illuminate\Support\Facades\Facade;

class RepeatHelperFacade extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'repeatHelper';
    }
}