<?php namespace App\Facade;

/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 3/2/16
 * Time: 12:53 PM
 */
class ResponseHelper
{
    const SUCCESS = 200;
    const FAIL = 400;
    const UNAUTHORIZED = 401;
    const NOT_FOUND = 404;

    private $status;
    private $data;
    private $message;
    private $statusCode;


    public function fail( $data = [], $message = "", $statusCode = self::FAIL){
        $this->status = 'fail';
        $this->data = $data;
        $this->message = $message;
        $this->statusCode = $statusCode;
        return $this->send();
    }



    public function success( $data = [],$message = "", $statusCode = self::SUCCESS){
        $this->status = 'success';
        $this->data = $data;
        $this->message = $message;
        $this->statusCode = $statusCode;
        return $this->send();
    }

    public function send(){
        
        $response = [];

        if( is_object( $this->data ) || ( is_array( $this->data ) && count( $this->data ) != 0 ) )
        {
            $response['data'] = $this->data;
        }

        if(!empty($this->message))
        {
            $response['message'] = $this->message;
        }

        return response()->json( array_merge([ 'status'=> $this->status], $response), $this->statusCode);
    }
}