<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 4/9/16
 * Time: 7:15 PM
 */

namespace App\Facade;
use App\Entities\Task;

class RepeatHelper
{
    protected $repo;

    public function __construct()
    {
        $this->repo = app()->make('App\Repos\Interfaces\Task');
    }

    public function getRepeatingDates( $task )
    {
        if( $task->repeat_code == 0 )
        {
            return null;
        }

        list( $repeatDaysBeforeDue, $totalDays ) = explode( '-' , $task->repeat_code );

        $baseDate = ( is_null($task->due_date) || strtotime($task->due_date) < time() ) ? $task->execution_time : $task->due_date;

        $data['repeat_date'] = ( $totalDays == 1 ) ? false  : date( 'Y-m-d H:i:s' , strtotime( "+ $repeatDaysBeforeDue day", strtotime($baseDate) ) );

        $data['due_date'] = date('Y-m-d H:i:s', strtotime("+ $totalDays day", strtotime( $baseDate ) ) );

        return $data;
    }

    public function onRepeatingDay( $task )
    {
        return $this->repo->makeDuplicate( $task );
    }

    public function afterCompletion( $task )
    {
        $dates = $this->getRepeatingDates( $task );

        $task = $this->repo->afterTaskCompletion( $task, $dates );

        if( $task->next_repeat === false )
        {
           $this->onRepeatingDay( $task );

           return true;
        }
        
        return true;
    }
}    