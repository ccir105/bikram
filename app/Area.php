<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'areas';

    protected $fillable = ['name'];

    public function shops(){
    	return $this->hasMany('App\Shop','area_id','id');
    }
}
