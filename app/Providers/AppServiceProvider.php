<?php

namespace App\Providers;

use App\Facade\RepeatHelper;
use Illuminate\Support\ServiceProvider;
use App\Facade\ResponseHelper;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind( 'App\Repos\Interfaces\Task', 'App\Repos\Eloquent\Task');

        $this->app->bind( 'App\Repos\Interfaces\Reminder', 'App\Repos\Eloquent\Reminder');

        $this->app->bind( 'App\Repos\Interfaces\Resource', 'App\Repos\Eloquent\Resource');

        $this->app->bind('responseHelper',function()
        {
            return new ResponseHelper();
        });

        $this->app->bind('repeatHelper',function()
        {
            return new RepeatHelper();
        });
    }
}
