<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Really Simple HTML Email Template</title>
<style>
/* -------------------------------------
    GLOBAL
------------------------------------- */
* {
  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
  font-size: 100%;
  line-height: 1.6em;
  margin: 0;
  padding: 0;
}

img {
  max-width: 600px;
  width: auto;
}

body {
  -webkit-font-smoothing: antialiased;
  height: 100%;
  -webkit-text-size-adjust: none;
  width: 100% !important;
}


/* -------------------------------------
    ELEMENTS
------------------------------------- */
a {
  color: #348eda;
}

.btn-primary {
  Margin-bottom: 10px;
  width: auto !important;
}

.btn-primary td {
  background-color: #348eda; 
  border-radius: 25px;
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; 
  font-size: 14px; 
  text-align: center;
  vertical-align: top; 
}

.btn-primary td a {
  background-color: #348eda;
  border: solid 1px #348eda;
  border-radius: 25px;
  border-width: 10px 20px;
  display: inline-block;
  color: #ffffff;
  cursor: pointer;
  font-weight: bold;
  line-height: 2;
  text-decoration: none;
}

.last {
  margin-bottom: 0;
}

.first {
  margin-top: 0;
}

.padding {
  padding: 10px 0;
}


/* -------------------------------------
    BODY
------------------------------------- */
table.body-wrap {
  padding: 20px;
  width: 100%;
}

table.body-wrap .container {
  border: 1px solid #f0f0f0;
}


/* -------------------------------------
    FOOTER
------------------------------------- */
table.footer-wrap {
  clear: both !important;
  width: 100%;  
}

.footer-wrap .container p {
  color: #666666;
  font-size: 12px;
  
}

table.footer-wrap a {
  color: #999999;
}


/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1, 
h2, 
h3 {
  color: #111111;
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  font-weight: 200;
  line-height: 1.2em;
  margin: 40px 0 10px;
}

h1 {
  font-size: 36px;
}
h2 {
  font-size: 28px;
}
h3 {
  font-size: 22px;
}

p, 
ul, 
ol {
  font-size: 14px;
  font-weight: normal;
  margin-bottom: 10px;
}

ul li, 
ol li {
  margin-left: 5px;
  list-style-position: inside;
}

td,th {
  vertical-align: top;
}
/* ---------------------------------------------------
    RESPONSIVENESS
------------------------------------------------------ */

/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
  clear: both !important;
  display: block !important;
  Margin: 0 auto !important;

}

/* Set the padding on the td rather than the div for Outlook compatibility */
.body-wrap .container {
  padding: 20px;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
  display: block;
  margin: 0 auto;
  
}

/* Let's make sure tables in the content area are 100% wide */
.content table {
  width: 100%;
}

table.val td {
  padding-bottom: 20px; 

}

table.tbl-border {
    border-collapse: collapse;
}

table.tbl-border td {
  padding-left: 10px;
}
table.tbl-border th {
  padding-left: 10px;
  text-align: left;
}
</style>
</head>

<body bgcolor="#f6f6f6">

<!-- body -->
<table class="body-wrap" bgcolor="#f6f6f6">
  <tr>
    <td></td>
    <td class="container" bgcolor="#FFFFFF">

      <!-- content -->
      <div class="content">
      <table>
        <tr>
          <td>
            <img style="width:30%;" src="{{ asset('image/pab-logo.jpg') }}">
          </td>
        </tr>
      </table>

      <table class="val">
        <tr>
          <td><strong>Erfassungsdatum</strong></td>
          <td>{{date('d.m.Y H:i')}}</td>
          <td><strong>Liefertermin</strong></td>
          <td>{{ $email['deliveryDate'] }}</td>
        </tr>

        <tr>
          <td valign="top"><strong>Drucker</strong></td>
          <td>pab plotteam<br>
Standort: Mühlemoosweg 12<br>
6414 Oberarth</td>

          <td><strong>Auftraggeber</strong></td>
          <td>{{ $email['contactPerson'] }}</td>
        </tr>

        <tr>
          <td valign="top"><strong>Projekt</strong></td>
          <td>{{$email['projectName']}}</td>

          <td><strong>Bemerkung</strong></td>
          <td>{{isset($email['comment']) ? $email['comment'] : ""}}</td>
        </tr>
        <tr>
          <td><strong>Rechnungsadresse</strong></td>
          <td>{{isset($email['invoiceAddress']) ? $email['invoiceAddress'] : ''}}</td>
        </tr>
        <tr>
          <td valign="top"><strong>E-mail</strong>
          <td>{{$email['email']}}</td>  
          <td valign="top"><strong>Phone</strong>
          <td>{{$email['phone']}}</td>
        </tr>

      </table>

      <!-- Main table with border -->
      <div style="overflow-x:auto;">
        <table border="1" class="tbl-border">
          <tr bgcolor="#dcdcdc">
            <th>Dok.-Name</th>
            <th>Format</th>
            <th>Farbe</th>
            <th>Menge</th>
            <th>Falten</th>
          </tr>

          @foreach($email['files'] as $files)
              @if(isset($files['names']) && is_array($files['names']) && count($files['names']) > 0 )
            <tr>
              <td>
                
                @foreach($files['names'] as $name)
                  {{$name}} <br>
                @endforeach
              </td>

            <td>{{$files['format']}}</td>
              <td>{{ ( $files['colored'] == 'SW' ) ? 'Nein' : 'Ja'   }}</td>
              <td>{{ $files['quantity'] }}</td>
              <td>{{ isset($email['fold']) ? $email['fold'] : 'Nein' }}</td>
            </tr>
            @endif
            @endforeach

        </table>
      </div>
      </div>
      <!-- /content -->
      
    </td>
    <td></td>
  </tr>
</table>
<!-- /body -->

<!-- footer -->
<table class="footer-wrap">
  
</table>
<!-- /footer -->

</body>
</html>
