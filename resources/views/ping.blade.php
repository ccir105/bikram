<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ping</title>
	<meta name="key" value="{{ $token }}">
</head>
	
<style>
	#pingForm{
		margin: 0 auto;
	}
</style>

<body>

	<div>
		<form id="pingForm">
			<input required type="text" name="domain">
			<br>
			<br>
			<button type="submit">Ping</button>
		</form>

		<pre id="output">

		</pre>			
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.min.js"></script>
	
	<script>
		
		(function(){

			$('#pingForm').on('submit', function(ev){

				var domain = $(this).find('[name="domain"]').val();

				if( domain !== "" )
				{
					//get the domain part
					var regex = /(?:(?:https?:\/\/)?(?:www\.|(?!www))([^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,}))/;

					var matches = regex.exec(domain);

					if( matches && matches[1])
					{
						$.ajax({
							type : 'POST',
							url: "{{ url('ping') }}",
							data: $(this).serialize(),
							success: function(){
								console.log('Request Sent')
							}
						})
					}

				}

				ev.preventDefault();

			});


			var con = io.connect('http://www.rentrunning.com:8888/client' );

			con.on('connect', function(){

				con.emit('token:verify', $('meta[name="key"]').attr("value"));

				con.on('token:verified', function(){

					con.on('output:received', function(output){

						output = $('#output').text() + output;
						$('#output').text(output);

					});

				});

			});

		})();

	</script>

</body>
</html>